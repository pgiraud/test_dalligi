![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

This project's goal is to test [dalligi] which should provide a CI bridge from
GitLab to SourceHut.

The idea is to use SourceHut runner to run CI jobs when a commit is pushed to
GitLab.

[dalligi]: https://sr.ht/~emersion/dalligi/
